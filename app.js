const express = require('express');
const axios = require('axios')
const path = require('path');
const { all } = require('express/lib/application');
const bodyParser = require('body-parser');
const qs = require('qs');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const PORT = 8080;

var router = express.Router();

router.get('/', async (req, res) => {
  const dataTickets = await axiosGet(ticketUrl)
  const dataUsers = await axiosGet(userUrl)
  res.render('index', { ticket: dataTickets.data , user: dataUsers.data });
  
});

router.post('/', async (req, res) => {
  await axiosPost(ticketUrl, req.body.ticketName, req.body.userId)
  res.redirect('/')
})

app.use('/', router);

app.listen(PORT, function () {
  console.log('Listening on port ' + PORT);
});

//AXIOS
const ticketUrl = "https://web-help-request-api.herokuapp.com/tickets"
const userUrl = "https://web-help-request-api.herokuapp.com/users"

const axiosGet = async (url) => {
  try {
    const { data: response } = await axios.get(url);
    return response;
  } catch (err) {
    console.error(err);
  }
};

const axiosPost = async(url, subject, userId) => {
  try {await axios.post(url, qs.stringify({
    subject: subject,
    userId: userId,
  }), {
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
  })} 
  catch (err) {
    console.error(err)
  }
}


// const axiosDelete = async (url) => {
//   try {await axios.patch(url)}
//  catch(err){
//    console.error(err)
//  }
// }

// axiosDelete(`https://web-help-request-api.herokuapp.com/tickets/`)